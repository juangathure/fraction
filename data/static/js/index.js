function update_query_parameters(url, key, val) {
   uri = url
      .replace(RegExp("([?&]"+key+"(?=[=&#]|$)[^#&]*|(?=#|$))"), "&"+key+"="+encodeURIComponent(val))
      .replace(/^([^?&]+)&/, "$1?");
   return uri;
}

function handleFiltersSubmit (event) {
  event.preventDefault();

  let product = document.getElementById('product').value;
  let department = document.getElementById('department').value;

  let url = window.location.href;
  let productFiltered = false;
  let departmentFiltered = false
  if (product !== 'Product') {
    url = update_query_parameters(url, 'product', product)
    productFiltered = true
  }

  if (department !== 'Department') {
    url = update_query_parameters(url, 'department', department)
    departmentFiltered = true
  }

  if (!productFiltered && departmentFiltered) {
    window.location.href= '/'
  } else {
    window.location.href = url;
  }
}

function handlePageSelect(event) {
  let url = window.location.href;

  url = update_query_parameters(url, 'page', event.target.value);

  window.location.href = url;
}

function setFilterValues() {
  const urlParams = new URLSearchParams(window.location.search)
  const product = urlParams.get('product');
  const department = urlParams.get('department');
  document.getElementById('product').value = product !== null ? product : 'Product';
  document.getElementById('department').value = department !== null ? department : 'Department';
}

setFilterValues();

function setActivePage() {
  const urlParams = new URLSearchParams(window.location.search)
  const page = urlParams.get('page');
  if (page ===  null) {
    return
  }

  const pageNumberId = 'pageNumber' + page;
  document.getElementById(pageNumberId).classList.add('active');
}

setActivePage()
