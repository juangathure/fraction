from django.conf import settings

import pymongo


_CLIENT = pymongo.MongoClient(
    settings.MONGO_DB_HOST,
    settings.MONGO_DB_PORT,
    username=settings.MONGO_DB_USERNAME,
    password=settings.MONGO_DB_PASSWORD,
)
_FRACTION_DB = _CLIENT['fraction']


def bulk_insert(collection=None, data=[]):
    assert collection is not None, 'Please pass in valid value for collection'
    assert len(data) != 0, 'Please pass in valid data'

    _FRACTION_DB[collection]

    _FRACTION_DB[collection].insert_many(data)


def fetch_data(collection):
    data = _FRACTION_DB[collection].find(
        {},
        {
            "Department": True,
            "Product": True,
            "Sales": True,
            "COGS": True,
            "Profit": True,
            "Date": True,
        },
    )

    return data


def fetch_distinct(collection, column):
    data = _FRACTION_DB[collection].distinct(column)

    return data


def filter_data(collection, query):
    data = _FRACTION_DB[collection].find(query)

    return data


def fetch_all(collection):
    data = _FRACTION_DB[collection].find()

    return data
