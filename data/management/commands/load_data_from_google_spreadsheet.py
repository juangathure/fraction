import os

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials

import pandas as pd

from django.conf import settings
from django.core.management.base import BaseCommand

from data.mongo_db import bulk_insert


# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = '1d-OnGMQG8IlPIG2Ru2SmiH2n9klzMxivfb3mD3imRBE'
# sheet on spreadsheet mapped to collection on mongo db
SHEET_TO_COLLECTION = {'Financial Data': 'financial', 'HR Data': 'hr'}


def _read_credentials():
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    tokens_file = os.path.join(settings.BASE_DIR, 'token.json')
    credentials_file = os.path.join(settings.BASE_DIR, 'credentials.json')
    if os.path.exists(tokens_file):
        creds = Credentials.from_authorized_user_file('token.json', SCOPES)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(credentials_file, SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open(tokens_file, 'w') as token:
            token.write(creds.to_json())

    return creds


def _pull_spreadsheet_data(sheet_name=None):
    """
    Reads spreadsheet data
    """

    creds = _read_credentials()
    service = build('sheets', 'v4', credentials=creds)
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=SAMPLE_SPREADSHEET_ID, range=sheet_name).execute()
    values = result.get('values', [])

    return values


def _format_data(data=None):
    columns = data[0]
    # remove leading and trailing spaces
    columns = [column.strip() for column in columns]
    rows = data[1:]
    dataframe = pd.DataFrame(rows, columns=columns)
    data_as_dict = dataframe.to_dict('records')

    return data_as_dict


class Command(BaseCommand):
    help = 'Pulls data from a google spreadsheet and saves it to a nosql database (mongo)'

    def handle(self, *args, **options):

        for sheet, collection in SHEET_TO_COLLECTION.items():
            self.stdout.write('Processing sheet %s' % sheet)

            self.stdout.write('Downloading the data...')
            data = _pull_spreadsheet_data(sheet_name=sheet)
            formated_data = _format_data(data=data)
            self.stdout.write(self.style.SUCCESS('Done downloading'))

            self.stdout.write('Saving data to the database...')
            bulk_insert(collection=collection, data=formated_data)
            self.stdout.write(self.style.SUCCESS('Done saving data'))

        self.stdout.write(self.style.SUCCESS('Done loading data'))
