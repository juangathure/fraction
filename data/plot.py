import plotly.express as px
import plotly.graph_objects as go

import pandas as pd


from data.mongo_db import fetch_data


def _prepare_data():
    '''
    plot cost of goods sold and sales per month
    '''

    data = fetch_data('financial')

    data = list(data)

    data_frame = pd.DataFrame(data)

    data_frame['Sales'] = (
        data_frame['Sales'].replace({'\$': '', ',': ''}, regex=True).astype(float).astype(int)
    )
    data_frame['COGS'] = (
        data_frame['COGS'].replace({'\$': '', ',': ''}, regex=True).astype(float).astype(int)
    )
    data_frame['Profit'] = (
        data_frame['Profit']
        .replace({'\$': '', ',': '', '\(': '', '\)': '', '\-': 0.0}, regex=True)
        .astype(float)
        .astype(int)
    )

    data_frame = data_frame.drop(['_id', 'Department', 'Product', 'Profit'], axis=1)

    data_frame['Date'] = pd.to_datetime(data_frame['Date'])

    sum_by_month = data_frame.groupby(pd.Grouper(key='Date', freq='1M')).sum().reset_index()

    return sum_by_month


def plot():
    data_frame = _prepare_data()

    fig = px.line(data_frame, x='Date', y=['COGS', 'Sales'], title='Sales and COGS by Month')

    return fig.to_html()
