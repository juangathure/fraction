import csv

from django.shortcuts import render
from django.http import HttpResponse

from data.mongo_db import fetch_data, fetch_distinct, filter_data, fetch_all
from data.plot import plot

# Create your views here.


def index(request):
    _PAGE_SIZE = 15

    collection = 'financial'
    products = fetch_distinct(collection, 'Product')
    departments = fetch_distinct(collection, 'Department')

    page = None

    if request.GET:
        filters = {}

        product_filter = request.GET.get('product', None)
        department_filter = request.GET.get('department', None)
        if product_filter is not None:
            filters['Product'] = product_filter

        if department_filter is not None:
            filters['Department'] = department_filter

        page = request.GET.get('page')

        data = filter_data(collection, filters)

    else:
        data = fetch_data(collection)

    data = list(data)

    page_count = len(data) // _PAGE_SIZE

    if page and not page == '1':
        page = int(page)
        offset_to = _PAGE_SIZE * page
        offset_from = offset_to - (_PAGE_SIZE + 1)
        data = data[offset_from:offset_to]
    else:
        data = data[0:_PAGE_SIZE]

    graph = plot()
    context = {
        'financial': data,
        'products': list(products),
        'departments': list(departments),
        'page_count': range(1, 18),
        'graph': graph,
    }

    return render(request, 'index.html', context=context)


def export_to_csv(request):
    response = HttpResponse(
        content_type='text/csv', headers={'Content-Disposition': 'attachment; filename="data.csv"'},
    )
    data = fetch_all('financial')
    data = list(data)

    keys = data[0].keys()
    writer = csv.DictWriter(response, keys)
    writer.writeheader()
    writer.writerows(data)

    return response
