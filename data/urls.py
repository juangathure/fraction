from django.urls import path

from data.views import index, export_to_csv

urlpatterns = [
    path('', index, name='index'),
    path('export-to-csv', export_to_csv, name='export-to-csv'),
]
