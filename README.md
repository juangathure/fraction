# FRACTION DATA LOADER

### HOW TO RUN THE APPLICATION.

##### REQUIREMENTS
1. Credentials to the google api (store them in the project root folder as credentials.json).
2. Docker.

#### Steps to run project
1. Start Mongo db. `docker-compose up`
2. Create python virtual environment. `virtualenv -p /usr/bin/python3 venv/`
3. Activate virtualenv `. virtualenv/bin/activate`
4. Install requirements. `pip install -r requirements.txt`
5. Load spreadsheet data into the mongo db database. `python manage.py load_data_from_google_spreadsheet`
6. Start the application.

#### What could be improved.
1. Completely clean the data before storing to the database.
2. Containerize the app.
3. Add integration testing for data loading management command, views, mongo db operations and unit testing for the graph ploting.
4. Grab some secrets values from environment variables or an env file, instead of hardcoding them on the settings file.
